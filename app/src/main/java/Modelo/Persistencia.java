package Modelo;

import com.example.preexamenc2.Usuario;

public interface Persistencia {
    public void openDataBase();
    public void closeDataBase();
    public long insertUsuario(Usuario usuario);
}
